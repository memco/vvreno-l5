<?php namespace memco\menu;

use Illuminate\Support\Collection as LaravelCollection;

class HTMLEntity extends LaravelCollection implements HTMLEntityInterface
{
	public $tag;

	public function __construct($tag, $attributes = [])
	{
		$this->tag = $tag;
		$this->attributes = $attributes;
	}
}