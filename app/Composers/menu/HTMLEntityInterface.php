<?php namespace memco\menu;

abstract class HTMLEntityInterface
{
	public $tag;
	public $attributes;

	public function hasChildren();

	public function has($child);

	public function attributes()
	{
		$output ='';

		if(! empty($this->attributes))
		{
			foreach ($this->attributes as $key => $value) {
				$output .= " $key=\"$value\"";
			}
		}

		return $output;
	}

}