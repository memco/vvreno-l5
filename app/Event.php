<?php namespace vvreno;

use Illuminate\Database\Eloquent\Model;

class Event extends Model {

	protected $table = 'events';

	protected $dates = ['event_date', 'created_at', 'updated_at'];

	public static function validate($data) {
		$rules = array(
			'title' => 'required',
			'event_date' => 'required|date'
		);

		Validator::register('date', function($attribute, $value, $parameters)
		{
			// Check for a valid date
			try {
				$date = new DateTime($value);
			} catch (Exception $e) {
				return false;
			}

			return true;
		});

		$messages = array(
			'title_required' => 'The <a href="#:attribute">:attribute</a> is required',
			'event_date_required' => 'The <a href="#:attribute">:attribute</a> is required',
			'event_date_date' => 'The <a href="#:attribute">:attribute</a> does not appear to be valid.',
		);

		return Validator::make($data, $rules, $messages);
	}

}
