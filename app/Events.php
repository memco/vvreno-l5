<?php

class Events extends Eloquent {

	public static $table = 'events';

	/**
	 * Make the event_date date a formattable DateTime object.
	 */
	public function get_event_date() {
		return new DateTime($this->get_attribute('event_date'));
	}

	public function set_event_date($date) {
		$this->set_attribute('event_date',new DateTime($date));
	}

	public static function validate($data) {
		$rules = array(
			'title' => 'required',
			'event_date' => 'required|date'
		);

		Validator::register('date', function($attribute, $value, $parameters)
		{
			// Check for a valid date
			try {
				$date = new DateTime($value);
			} catch (Exception $e) {
				return false;
			}

			return true;
		});

		$messages = array(
			'title_required' => 'The <a href="#:attribute">:attribute</a> is required',
			'event_date_required' => 'The <a href="#:attribute">:attribute</a> is required',
			'event_date_date' => 'The <a href="#:attribute">:attribute</a> does not appear to be valid.',
		);

		return Validator::make($data, $rules, $messages);
	}

	public static function update($id,$data) {
		$event = Events::find($id);

		foreach($event->attributes as $attribute => $val) {
			if(isset($data[$attribute]))
				$event->$attribute = $data[$attribute];
		}
		$event->save();
	}
}