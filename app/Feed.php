<?php namespace vvreno;

use SimplePie;

class Feed {

	// public function __construct() {
	// 	// Include FFmpegPHP
	// 	// require_once path('app').'/libraries/simplepie.php';
	// }

	public static function get_sermon_net_sermons() {
		$cache_folder = storage_path().'/cache/sermonnet/';
		$feed = new SimplePie();
		$feed->set_feed_url('http://sermon.net/rss/client/valleyviewreno/type/audio');
		$feed->enable_cache(true);
		$feed->set_cache_duration(604800);
		$feed->set_cache_location($cache_folder);
		$feed->init();
		$feed->handle_content_type();

		return $feed;
	}

	public static function get_vvcf_blog() {
		$cache_folder = storage_path().'/cache/vvcf_blog/';
		$feed = new SimplePie();
		$feed->set_feed_url('http://valleyviewreno.blogspot.co.uk/feeds/posts/default');
		$feed->enable_cache(true);
		$feed->set_cache_duration(86400);
		$feed->set_cache_location($cache_folder);
		$feed->init();
		$feed->handle_content_type();

		return $feed;
	}

	public static function get_vimeo_channel() {
		$cache_folder = storage_path().'/cache/vvcf_vimeo/';
		$feed = new SimplePie();
		$feed->set_feed_url('https://vimeo.com/channels/881990/videos/rss');
		$feed->enable_cache(true);
		$feed->set_cache_duration(86400);
		$feed->set_cache_location($cache_folder);
		$feed->init();
		$feed->handle_content_type();

		return $feed;
	}

	public static function getFacebookFeed() {
		$cache_folder = storage_path().'/cache/vvcf_facebook/';
		$feed = new SimplePie();
		$feed->set_feed_url('https://www.facebook.com/feeds/page.php?format=rss20&id=187926878004898');
		$feed->enable_cache(true);
		$feed->set_cache_duration(86400);
		$feed->set_cache_location($cache_folder);
		$feed->init();
		$feed->handle_content_type();

		return $feed;
	}

	public static function resizeFacebookImages($post) {
		return str_replace('_n.jpg', '_o.jpg', $post);
	}

	public static function make_time_ago($start) {

		if(!($start instanceof DateTime)) {
	        $start = new DateTime($start);
	    }

        $end = new DateTime();

	    $interval = $end->diff($start);
	    $doPlural = function($nb,$str){return $nb>1?$str.'s':$str;}; // adds plurals

	    $format = array();
	    if($interval->y !== 0) {
	        $format[] = "%y ".$doPlural($interval->y, "year");
	    }
	    if($interval->m !== 0) {
	        $format[] = "%m ".$doPlural($interval->m, "month");
	    }
	    if($interval->d !== 0) {
	        $format[] = "%d ".$doPlural($interval->d, "day");
	    }
	    if($interval->h !== 0) {
	        $format[] = "%h ".$doPlural($interval->h, "hour");
	    }
	    if($interval->i !== 0) {
	        $format[] = "%i ".$doPlural($interval->i, "minute");
	    }
	    if($interval->s !== 0) {
	        if(!count($format)) {
	            return "less than a minute ago";
	        } else {
	            $format[] = "%s ".$doPlural($interval->s, "second");
	        }
	    }

	    $format = array_shift($format);

	    return $interval->format($format).' ago';

	}

	public static function get_sermon_id($sermon) {
		return mb_substr($sermon->get_id(),42,10);
	}
}