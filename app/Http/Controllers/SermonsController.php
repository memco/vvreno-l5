<?php namespace vvreno\Http\Controllers;

use vvreno\Http\Requests;
use vvreno\Http\Controllers\Controller;

use Illuminate\Http\Request;

use vvreno\Sermon;

class SermonsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sermons = Sermon::orderBy('preached_on', 'DESC')->paginate(20);

		return view('app', ['content' => view('sermon.index', compact('sermons'))])
			->with('title', 'Sermons');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('app', ['content' => view('sermon.add', compact('sermons'))])
			->with('title', 'Upload Sermon');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
