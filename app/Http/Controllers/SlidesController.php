<?php namespace vvreno\Http\Controllers;

// use vvreno\Http\Requests;
use vvreno\Http\Controllers\Controller;

use vvreno\Slide;
use vvreno\Http\Requests\StoreSlideRequest;

use Illuminate\Http\Request;

use vvreno\Alert;

class SlidesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('app')
			->with('content', view('slide.index', ['slides'=>Slide::all()]))
			->with('title', 'Slides');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('app')
			->with('content', view('slide.add', ['slides'=>Slide::all()]))
			->with('title', 'Slides');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StoreSlideRequest $request)
	{
		// \Debugbar::info("StoreSlideRequest succeeded");

		if($request->hasFile('file'))
		{
			$name = $request->file('file')->getClientOriginalName();
			$path = $request->file('file')->getPathname() . $name;
		}
		else
		{
			$path = $request->get('url');
			$name = basename(parse_url($path, PHP_URL_PATH));
		}

		try
		{
			$img = \Image::make($path);
		}
		catch(\Exception $e)
		{
			Alert::add('error','Failed to create image');
			return \Redirect::back();
		}


		$storageDir = storage_path().'/app/images/slides/';

		$img->save($storageDir.$name);

		Slide::create(array_merge($request->all(),['path'=>$name]));

		\Alert::add('success', 'Slide saved!');
		return \Redirect::to('slides');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return view('app')
			->with('content', view('slide.edit', ['slide'=>Slide::find($id)]))
			->with('title', 'Slides');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
