<?php namespace vvreno\Http\Controllers;

use vvreno\Http\Requests;
use vvreno\Http\Controllers\Controller;

use Illuminate\Http\Request;

use vvreno\Slogan;

class SloganController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit()
	{
		$slogan = Slogan::first();
		return view('app', ['content' => view('slogan.edit', compact('slogan'))]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request request
	 * @return Response
	 */
	public function update(Request $request)
	{
		$slogan = Slogan::first();
		$slogan->slogan = $request->input('slogan');
		$slogan->save();

		return redirect('/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
