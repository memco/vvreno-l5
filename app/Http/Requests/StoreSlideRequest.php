<?php namespace vvreno\Http\Requests;

use vvreno\Http\Requests\Request;

class StoreSlideRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if(\Auth::check()) return true;

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'file' => 'required_without:url|mimes:jpeg,png,gif,tif,bmp,ico,psd',
			'url' => 'required_without:file'
		];
	}

}
