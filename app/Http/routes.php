<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function()
{
	return view('app')->with('content', view('home'));
});

Route::get('about', function()
{
	return View::make('app')
		->with('title','About Us')
		->with('content',View::make('about'));
});

Route::get('pastors', function()
{
	return Redirect::to('about');
});

Route::get('divorce-care', function()
{
	return View::make('app')
		->with('title', 'Divorce Care')
		->with('content', View::make('divorce-care'));
});

Route::get('celebrate-recovery', function()
{
	return View::make('app')
		->with('title', 'Celebrate Recovery')
		->with('content', View::make('celebrate-recovery'));
});

Route::get('baptism', function()
{
	return View::make('app')
		->with('title', 'Baptism')
		->with('content', View::make('baptism'));
});

Route::get('give', function(){
	return View::make('app')
		->with('title', 'Giving')
		->with('content', View::make('giving'));
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('img/{path}', function(League\Glide\Server $server, Illuminate\Http\Request $request, $path)
{
	$server->outputImage($path, Input::all());
})->where('path', '.*');

Route::get('slogan', 'SloganController@edit');
Route::put('slogan', 'SloganController@update');

Route::resource('slides', 'SlidesController');
Route::resource('sermons', 'SermonsController');


// Non-static pages; using controllers
// Sermons
// Route::any('sermons',array('as'=>'sermons','uses'=>'sermon@index'));
// Route::any('sermons/create', array('before'=>'auth','as'=>'sermons/create','uses'=>'sermon@add'));
// Route::any('sermons/(:num)/show',array('as'=>'sermons/show','uses'=>'sermon@view'));
// Route::any('sermons/(:num)/edit', array('before'=>'auth','as'=>'sermons/edit','uses'=>'sermon@edit'));
// Route::any('sermons/(:num)/delete', array('before'=>'auth','as'=>'sermons/delete','uses'=>'sermon@delete'));

// // Events
// Route::any('events',array('as'=>'events','uses'=>'events@index'));
// Route::any('events/create', array('before'=>'auth','as'=>'events/create','uses'=>'events@add'));
// Route::any('events/(:num)/show',array('as'=>'events/show','uses'=>'events@view'));
// Route::any('events/(:num)/edit', array('before'=>'auth','as'=>'events/edit','uses'=>'events@edit'));
// Route::any('events/(:num)/delete', array('before'=>'auth','as'=>'events/delete','uses'=>'events@delete'));

// // Slides
// Route::any('slides',array('before'=>'auth','as'=>'slides','uses'=>'slide@index'));
// Route::any('slides/create', array('before'=>'auth','as'=>'slides/create','uses'=>'slide@add'));
// Route::any('slides/(:num)/show',array('before'=>'auth','as'=>'slides/show','uses'=>'slide@view'));
// Route::any('slides/(:num)/edit', array('before'=>'auth','as'=>'slides/edit','uses'=>'slide@edit'));
// Route::any('slides/(:num)/delete', array('before'=>'auth','as'=>'slides/delete','uses'=>'slide@delete'));

// // Slogans
// Route::any('slogans',array('before'=>'auth','as'=>'slogans','uses'=>'slogan@index'));
// Route::any('slogans/create', array('before'=>'auth','as'=>'slogans/create','uses'=>'slogan@add'));
// Route::any('slogans/(:num)/show',array('before'=>'auth','as'=>'slogans/show','uses'=>'slogan@view'));
// Route::any('slogans/(:num)/edit', array('before'=>'auth','as'=>'slogans/edit','uses'=>'slogan@edit'));
// Route::any('slogans/(:num)/delete', array('before'=>'auth','as'=>'slogans/delete','uses'=>'slogan@delete'));

// // Users
// Route::any('users',array('before'=>'auth','as'=>'users','uses'=>'user@index'));
// Route::any('users/create', array('before'=>'auth','as'=>'users/create','uses'=>'user@add'));
// Route::any('users/(:num)/show',array('before'=>'auth','as'=>'users/show','uses'=>'user@view'));
// Route::any('users/(:num)/edit', array('before'=>'auth','as'=>'users/edit','uses'=>'user@edit'));
// Route::any('users/(:num)/delete', array('before'=>'auth','as'=>'users/delete','uses'=>'user@delete'));
// Route::any('login',array('as'=>'login','uses'=>'user@login'));
// Route::any('logout',array('as'=>'logout','uses'=>'user@logout'));

Route::get('gcal', function()
{
	var_dump(Gcal::get());
	die;
});

View::composer('home', function($view)
{
// 	$view->with('sermons',Sermon::where('published','=',1)->order_by('preached_on','DESC')->take(1)->get());
	$view->with('blog',vvreno\Feed::get_vvcf_blog()->get_items(0,1));
// 	$view->with('events',Events::where('event_date','>=',new DateTime())->order_by('event_date', 'ASC')->take(2)->get());
	$view->with('tweets', Twitter::getUserTimeline(['screen_name' => 'theviewreno', 'count' => 1]));
	$view->with('slides',vvreno\Slide::all());
	$view->with('videos',vvreno\Feed::get_vimeo_channel()->get_items(0,1));
	// $view->with('facebook',vvreno\Feed::getFacebookFeed()->get_items(0,3));
});

View::composer('app', function($view)
{

	$siteMenu = Menu::handler('site')
		->add('about', 'About')
		->add('sermons', 'Sermons')
		->add('#', 'Ministries', Menu::items()
			->add('baptism', 'Baptism')
			->add('celebrate-recovery', 'Celebrate Recovery')
			->add('http://dimensionsreno.org/','Dimensions')
			->add('divorce-care', 'Divorce Care')
			->add('tribe', 'Tribe')
			, [], ['class'=>'submenu'])
		->add('give', 'Give');
	$view->with('menu', $siteMenu);

	$admin_menu = Menu::handler('menu-admin')
		->add('events','Events', Menu::items()
			->add('events','View Events')
			->add('events/create','New Event'))

		->add('sermons','Sermons', Menu::items()
			->add('sermons','View Sermons')
			->add('sermons/create','New Sermon'))

		->add('slides','Slides', Menu::items()
			->add('slides','View Slides')
			->add('slides/create','New Slide'))

		->add('slogans', 'Slogans', Menu::items()
			->add("slogan", 'Edit Slogan')) // Only one slogan so we cheat here

		->add('users','Users', Menu::items()
			->add('users','View Users')
			->add('users/create','New User'))

		->add('logout','Log out');
	$view->with('adminMenu',$admin_menu);

	$view->with('slogan', \vvreno\Slogan::first()->slogan);

	if(isset($view->errors) && $view->errors->has())
	{
		$errorCount = count($errors = $view->errors->all('<li>:message</li>'));
		$msg = "<p>Please review the $errorCount issue(s) found:</p>\n<ul>\n";
		foreach($errors as $error) {
			$msg .= $error;
		}
		$msg .= "</ul>";

		Alert::add('error',$msg);
	}
});