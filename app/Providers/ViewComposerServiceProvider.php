<?php namespace vvreno\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        // View::composer('app', 'vvreno\Http\Composers\MenuComposer');
    }

    public function register()
    {

    }
}