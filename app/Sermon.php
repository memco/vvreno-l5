<?php namespace vvreno;

use Illuminate\Database\Eloquent\Model;

class Sermon extends Model {

	protected $dates = ['preached_on', 'created_at', 'updated_at'];

	public function getLengthAttribute($length) {
		$length = (int) $length;		// length cannot have decimal values
		if(! $length instanceof Mydateinterval)
		{
			$length = new Mydateinterval("PT{$length}S");
		}
		return $length->recalculate();
	}

	/**
	 * Validate an uploaded file meets our requirements
	 */

	public static function validate_audio_file($file) {
		Validator::register('type', function($attribute, $value, $parameters)
		{
			// Check for a MIME file category (audio, video, etc.)
			// This may not be secure, please use additional processing on the file before using

			foreach ($parameters as $type)
			{
				if ( strncmp($value['type'],$type,strlen($type)-1) == 0)
				{
					return true;
				}
			}

			return false;
		});

		$rules = array(
			'file' => 'required|type:audio,video',
		);

		$messages = array(
			'file_type' => 'This file type is not supported.'
		);

		return Validator::make($file, $rules, $messages);
	}

	public static function add($file) {
		require_once path('app')."libraries/ffmpeg-php/FFmpegAutoloader.php";

		$audio_dir = path('public')."audio/";
		$file_path = "$audio_dir{$file['name']}";
		$upload = Input::upload('file', $audio_dir, $file['name']);


		if(! $data = new FFmpegMovie($file_path, new FFmpegOutputProvider(Config::get('application.ffmpeg')))) {
			return false;
		}

		$sermon = new Sermon();
		$sermon->title = $data->getTitle();
		$sermon->speakers = $data->getArtist();
		$sermon->path = $file_path;
		$sermon->url = asset('audio/').rawurlencode($file['name']);
		$sermon->length = $data->getDuration();
		$sermon->size = round($file['size']/1048576,1)." MB";
		$sermon->preached_on = new DateTime();
		$sermon->mime = $upload->getMimeType();
		$sermon->save();
		return $sermon;
	}

	// public function update($id,$data) {
	// 	$sermon = Sermon::find($id);

	// 	// Pre-set published option
	// 	$data['published'] = isset($data['published']) ? 1 : 0;

	// 	foreach($sermon->attributes as $attribute => $val) {
	// 		if(isset($data[$attribute]))
	// 			$sermon->$attribute = $data[$attribute];
	// 	}
	// 	$sermon->save();
	// }

	// public function delete() {
	// 	// Will be used for custom cleanup.  Not deleting files currently, but might.

	// 	// File::delete($sermon->path);
	// 	parent::delete();
	// }

	public function scan()
	{
		// Command::run(['']);
	}
}