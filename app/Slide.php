<?php namespace vvreno;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model {
  protected $storageDir = 'img/slides/';

  protected $rules = [];

  protected $fillable = ['heading', 'path', 'url', 'link', 'description'];

  public $acceptedMimes = [
    'image/jpeg' => 'jpg',
    'image/gif' => 'gif',
    'image/png' => 'png',
    'image/tiff' => 'tif',
    'image/bmp' => 'bmp',
    'image/x-icon' => 'ico',
    'application/octet-stream' => 'psd'
  ];

  public static function mimes()
  {
    return $this->acceptedMimes;
  }

 //  public function get_url($size = null)
 //  {
 //    if($size !== null)
 //    {
 //      $bits = pathinfo(parse_url($this->get_attribute('url'), PHP_URL_PATH));
 //      return URL::to_asset(self::$storageDir.$bits['filename']."-$size.".$bits['extension']);
 //    }

 //    return $this->get_attribute('url');
 //  }

	// public static function validate_create($data)
 //  {
 //    Validator::register('type', function($attribute, $value, $parameters)
 //    {
 //      // Check for a MIME file category (audio, video, etc.)
 //      // This may not be secure, please use additional processing on the file before using

 //      foreach ($parameters as $type)
 //      {
 //        if ( strncmp($value['type'],$type,strlen($type)-1) == 0)
 //        {
 //          return true;
 //        }
 //      }

 //      return false;
 //    });

	// 	$rules = array(
	// 		'file' => 'required|mimes:gif,jpeg,pjpeg,png',
 //      'link' => 'active_url'
	// 	);

	// 	$messages = array(
	// 		'file_mimes' => 'This file type is not supported. Images should be in gif, jpg or png format.',
 //      'link_active_url' => 'This <a href=":attribute">link</a> appears to be broken.'
	// 	);

	// 	return Validator::make($data, $rules, $messages);
	// }

 //  public static function validate_edit($data)
 //  {
 //    Validator::register('type', function($attribute, $value, $parameters)
 //    {
 //      // Check for a MIME file category (audio, video, etc.)
 //      // This may not be secure, please use additional processing on the file before using

 //      foreach ($parameters as $type)
 //      {
 //        if ( strncmp($value['type'],$type,strlen($type)-1) == 0)
 //        {
 //          return true;
 //        }
 //      }

 //      return false;
 //    });

 //    $rules = array(
 //      'file' => 'mimes:gif,jpeg,pjpeg,png',
 //      'link' => 'active_url'
 //    );

 //    $messages = array(
 //      'file_mimes' => 'This <a href=":attribute">file</a> type is not supported. Images should be in gif, jpg or png format.',
 //      'link_active_url' => 'This <a href=":attribute">link</a> appears to be broken.'
 //    );

 //    return Validator::make($data, $rules, $messages);
 //  }

 //  /**
 //   * Allow creation of a slide for the homepage.  Can accept an uploaded file or
 //   * can take a URL to an image.
 //   * @param mixed $data
 //   */
 //  public static function create($data)
 //  {
 //    $slide = new Slide();

 //    // If the user uploaded a file instead of providing a URL
 //    if(Input::has_file('file')) {
 //      File::mkdir(path('public').self::$storageDir,0755);
 //      Input::upload('file', path('public').self::$storageDir, $data['file']['name']);
 //      $slide->path = path('public').self::$storageDir.$data['file']['name'];
 //      $slide->url = URL::to_asset(self::$storageDir).$data['file']['name'];
 //    }

 //    // TODO: implement URL based slide creation

 //    $optional = array('link','description');
 //    foreach ($optional as $option) {
 //      if($data[$option] && ! empty($data[$option])) {
 //        $this->description = $data[$option];
 //      }
 //    }

 //    $result = $slide->save();

 //    return ($result) ? $slide : false;
 //  }

 //  public static function update($id, $data)
 //  {
 //    $slide = Slide::find($id);

 //    if(Input::has_file('file')) {
 //      File::mkdir(path('public').self::$storageDir,0755);
 //      Input::upload('file', path('public').self::$storageDir, $data['file']['name']);
 //      $slide->path = path('public').self::$storageDir.$data['file']['name'];
 //      $slide->url = URL::to_asset(self::$storageDir).$data['file']['name'];
 //    }

 //    $info = pathinfo($data['file']['name']);
 //    $slide->heading = $data['heading'] ? $data['heading'] : $info['filename'];

 //    $optional = array('link','description');
 //    foreach ($optional as $option) {
 //      if($data[$option] && ! empty($data[$option])) {
 //        $this->description = $data[$option];
 //      }
 //    }

 //    $slide->save();
 //  }
}