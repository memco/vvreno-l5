<?php

class CreateSermonsTable {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		//

		Schema::create('sermons', function($table)
		{

			$table->engine = 'InnoDB';

		    $table->increments('id');

		    // Message title
		    $table->string('title');

		    // Name(s) of speaker(s)
		    $table->string('speakers');

		    // Storage path of sermon mp3
		    $table->string('path');

		    // Web accessible URL
		    $table->string('url');

		    // Size of file in MB
		    $table->string('size');

		    // Duration of file
		    $table->string('length');

		    // Date preached
		    $table->date('preached_on');

		    // File's source (sermon.net or upload)
		    $table->string('source');

		    $table->text('description');
		    $table->string('tags');

		    // Toggle public visibility
		    $table->boolean('published');

		    $table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		//

		Schema::drop('sermons');
	}

}