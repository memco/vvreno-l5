<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// $this->call('UserTableSeeder');
		// $this->call('SloganTableSeeder');
		// $this->call('SlideTableSeeder');
		// $this->call('SermonTableSeeder');
		// $this->call('EventsTableSeeder');
	}


	/**
	 * Import data from CSVS.
	 *
	 * Expects first row to be headers.
	 *
	 * @param  String $file absolute path to file to import
	 * @param  String $model name of model to use for new records
	 * @return Bool
	 */
	public static function importCSV($file, $model)
	{
		if(!file_exists($file) || ! class_exists($model) || !method_exists($model, 'firstOrCreate'))
		{
			return false;
		}

		$handle = fopen($file, 'r');
		$headers = fgetcsv($handle);

		while($row = fgetcsv($handle))
		{
			$data = array_combine($headers, $row);

			// Pseudo non-destructive seeding: allows seeding to only import new data
			$model::firstOrCreate($data);
		}

		fclose($handle);
		return true;
	}

}

class SloganTableSeeder extends Seeder {
	public function run()
	{
		DB::table('slogans')->delete();
		$this->command->info('Slogan cleared');
		vvreno\Slogan::create(['slogan'=>'']);
	}
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();
        $this->command->info('User table cleared');

        $password = $this->command->secret('What is the password?');

        vvreno\User::create(['email' => 'mikemckend@gmail.com', 'password'=>Hash::make($password), 'name'=>'michael']);
    }

}

class SermonTableSeeder extends Seeder {

	public function run()
	{
		$file = 'database/seeds/sermons.csv';
		$model = 'vvreno\Sermon';
		DatabaseSeeder::importCSV($file, $model);
	}

}

class SlideTableSeeder extends Seeder {

	public function run()
	{
		$file = 'database/seeds/slides.csv';
		$model = 'vvreno\Slide';
		DatabaseSeeder::importCSV($file, $model);
	}

}

class EventsTableSeeder extends Seeder {

	public function run()
	{
		$file = 'database/seeds/events.csv';
		$model = 'vvreno\Event';
		DatabaseSeeder::importCSV($file, $model);
	}

}