var elixir = require("laravel-elixir");
var gulp = require("gulp");

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.sass('sass.scss', 'resources/assets/css/');

	mix.styles([
		'css/sass.css',
		'css/bss-slides.css',
		'libraries/flexslider/flexslider.css',
		'libraries/timepicker/jquery-ui-timepicker-addon.css',
		'libraries/mediaelement/mediaelementplayer.min.css',
		'libraries/plupload/js/jquery.ui.plupload/css/jquery.ui.plupload.css',
		'libraries/plupload/js/jquery.pluploadp.queue/css/jquery.plupload.queue.css'
	], 'public/css/styles.css', 'resources/assets');

	mix.scripts([
		'js/menu.js',
		'js/bss-slides.js',
		'libraries/plupload/js/moxie.min.js',
		'libraries/plupload/js/plupload.min.js',
		'libraries/flexslider/jquery.flexslider-min.js',
		'libraries/timepicker/jquery-ui-timepicker-addon.js',
		'libraries/mediaelement/build/mediaelement-and-player.min.js',
		'libraries/plupload/js/jquery.ui.plupload/jquery.ui.plupload.min.js',
		'libraries/plupload/js/jquery.pluploadp.queue/jquery.plupload.queue.min.js'
	], 'public/js/scripts.js', 'resources/assets');

	mix.copy('resources/assets/libraries/plupload', 'public/libraries/plupload');
	mix.copy('resources/assets/libraries/plupload/jquery.ui.plupload/img', 'storage/app/images/libraries/plupload/jquery.ui.plupload/img');
	mix.copy('resources/assets/libraries/plupload/jquery.plupload.queue/img', 'storage/app/images/libraries/plupload/jquery.plupload.queue/img');

	mix.version(['public/js/scripts.js', 'public/css/styles.css']);
});