/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'fontawesome\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-warning-sign' : '&#xf071;',
			'icon-info-sign' : '&#xf05a;',
			'icon-question-sign' : '&#xf059;',
			'icon-ok-circle' : '&#xf05d;',
			'icon-play' : '&#xf04b;',
			'icon-backward' : '&#xf04a;',
			'icon-fast-backward' : '&#xf049;',
			'icon-step-backward' : '&#xf048;',
			'icon-pause' : '&#xf04c;',
			'icon-stop' : '&#xf04d;',
			'icon-forward' : '&#xf04e;',
			'icon-fast-forward' : '&#xf050;',
			'icon-step-forward' : '&#xf051;',
			'icon-chevron-left' : '&#xf053;',
			'icon-chevron-right' : '&#xf054;',
			'icon-plus-sign' : '&#xf055;',
			'icon-minus-sign' : '&#xf056;',
			'icon-ok-sign' : '&#xf058;',
			'icon-map-marker' : '&#xf041;',
			'icon-cog' : '&#xf013;',
			'icon-home' : '&#xf015;',
			'icon-trash' : '&#xf014;',
			'icon-remove' : '&#xf00d;',
			'icon-ok' : '&#xf00c;',
			'icon-th-list' : '&#xf00b;',
			'icon-search' : '&#xf002;',
			'icon-envelope' : '&#xf003;',
			'icon-download-alt' : '&#xf019;',
			'icon-time' : '&#xf017;',
			'icon-exclamation-sign' : '&#xf06a;',
			'icon-cogs' : '&#xf085;',
			'icon-phone-sign' : '&#xf098;',
			'icon-google-plus-sign' : '&#xf0d4;',
			'icon-caret-right' : '&#xf0da;',
			'icon-caret-down' : '&#xf0d7;',
			'icon-caret-up' : '&#xf0d8;',
			'icon-caret-left' : '&#xf0d9;',
			'icon-sort' : '&#xf0dc;',
			'icon-sort-down' : '&#xf0dd;',
			'icon-sort-up' : '&#xf0de;',
			'icon-envelope-alt' : '&#xf0e0;',
			'icon-sitemap' : '&#xf0e8;',
			'icon-volume-off' : '&#xf026;',
			'icon-volume-down' : '&#xf027;',
			'icon-volume-up' : '&#xf028;',
			'icon-facebook-sign' : '&#xf082;',
			'icon-twitter-sign' : '&#xf081;',
			'icon-ban-circle' : '&#xf05e;',
			'icon-remove-circle' : '&#xf05c;',
			'icon-plus' : '&#xf067;',
			'icon-sign-blank' : '&#xf0c8;',
			'icon-sign-blank-2' : '&#xe004;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};