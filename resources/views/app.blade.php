<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ isset($title) ? "$title | " : '' }}Valley View Reno</title>
      {{ isset($pageDescription) ? '<meta name="description" content="'.$pageDescription.'">' : '' }}

	<link rel="stylesheet" type="text/css" href="{{ elixir('css/styles.css') }}">
	<link rel="icon" type="image/ico" href="{{ asset('favicon.ico') }}">

	<!-- Fonts -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<nav class="visually-hidden" id="menu-access">
		<a href="#content">Skip to page content</a>
		<a href="#menu-site">Skip to site navigation menu</a>
		<a href="#menu-social">Skip to social media links and email</a>
	</nav>

	<header id="site-header">
		@if(isset($adminMenu))
			<nav id="menu-admin">
				{!! $adminMenu !!}
			</nav>
		@endif
		<div id="header-content">
			<a href="{!! url('/') !!}"><img src="{!! asset('img/logo-HD.png') !!}" alt="Valley Veiw Reno" id="logo"></a>
			<h1 id="slogan">{!! $slogan !!}</h1>
			<nav id="menu-site">
				{!! $menu !!}
			</nav>
			<nav id="menu-social">
				<a href="https://plus.google.com/110816751180890279304/posts" title="Valley View Reno on Google+"><span aria-hidden="true" data-icon="&#xf0d4;"><span class="visually-hidden">Google+</span></a>
				<a href="https://twitter.com/theviewreno" title="Valley View Reno on Twitter"><span aria-hidden="true" data-icon="&#xf081;"></span><span class="visually-hidden">Twitter</span></a>
				<a href="https://www.facebook.com/vvcfreno" title="Valley View Reno on Facebook"><span aria-hidden="true" data-icon="&#xf082;"></span><span class="visually-hidden">Facebook</span></a>
				<a href="mailto:mail@valleyviewreno.org" title="Email Valley View"><span aria-hidden="true" data-icon="&#xe004;"></span><span class="visually-hidden">Email</span></a>
			</nav>
		</div>
	</header>

	<section id="content">
		{!! vvreno\Alert::render('partials.alerts') !!}

		{!! $content !!}
	</section>

	<!-- Scripts -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>

	<script src="{{ elixir('js/scripts.js') }}"></script>

	@if($app->environment('production'))
		<script defer>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-40502605-1', 'valleyviewreno.org');
			ga('send', 'pageview');
		</script>
	@endif

	<script src="https://raw.githubusercontent.com/hammerjs/hammer.js/master/hammer.min.js"></script><!-- for swipe support on touch interfaces -->
	{{-- <script src="https://raw.githubusercontent.com/leemark/better-simple-slideshow/gh-pages/js/better-simple-slideshow.min.js"></script> --}}

	<script defer type="text/javascript">
            $(document).ready( function() {

                // Mediaelement
               $('audio,video').mediaelementplayer({
                // width of audio player
                // audioWidth: '99%', // for some reason 100% causes volume slider to overflow
                // force iPad's native controls
                iPadUseNativeControls: true,
                // force iPhone's native controls
                iPhoneUseNativeControls: true,
                // force Android's native controls
                AndroidUseNativeControls: true,
                });

               // Date picker
               $('.date-picker, input[type="date"]').datepicker({
                    minDate: 0,
               });

               $('.datetime-picker, input[type="datetime"]').datetimepicker({
                    minDate: 0,
                    timeFormat: 'h:mm TT',
                    controlType: 'select'
               });

               // Slides
               // $('#slideshow').flexslider();

               var opts = {
                     //auto-advancing slides? accepts boolean (true/false) or object
                     auto : {
                         // speed to advance slides at. accepts number of milliseconds
                         speed : 2500,
                         // pause advancing on mouseover? accepts boolean
                         pauseOnHover : true
                     },
                     // show fullscreen toggle? accepts boolean
                     fullScreen : true,
                     // support swiping on touch devices? accepts boolean, requires hammer.js
                     swipe : true
                 };
			makeBSS('.bss-slides', opts);
            });
        </script>
</body>
</html>
