<h1>Water Baptism for Children</h1>
<iframe src="//player.vimeo.com/video/77333363?color=ada28e" width="1104" height="621" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p>We are so excited that you are considering baptism for your child. Every child can have a personal relationship with Jesus. In Kids Life, we encourage and give opportunities for children to make a decision to follow Jesus. Our children are encouraged to not just learn about Jesus, but learn from Jesus by walking with Him every day.
We advise parents to wait until their child is asking to be water baptized. This is usually a good indication that they are ready to be baptized and it will be a spiritual milestone in their life. However, as a point of inspiration, it may be helpful. In our faith tradition people get baptized many times, for many reasons, including re-dedication.</p>
<p>As you discern if your child is ready to be water baptized, here are some questions to ask your child:</p>
<ol>
	<li>
		<h2>Why do you want to be water baptized?</h2>
		<p>Answers that indicate that your child understands: as an example: “Because Jesus was and he tells us to also; Because I’m a friend of Jesus and I want to follow Him.”</p>
	</li>
	<li>
		<h2>What does it mean to be water baptized?</h2>
		<p>Answers that indicate your child understands: It means I’m following Jesus; It means I’m a Christian and I want to show the world; It’s a picture of what Jesus has done on the inside of me.</p>
	</li>
	<li>
		<h2>When did you decide to follow Jesus and become His forever friend (born again, saved)?</h2>
		<p>Answers that indicate your child understands: Your child should have a specific memory of when he understood what Jesus did for him and that he needs Jesus in his life. It may have been at church, at home, or even sometime when he was alone.</p>
	</li>
</ol>

<h2>Your Child Is Ready:</h2>
<p>If you feel your child understands what it means to be water baptized, then go through this devotional explaining more about water baptism.</p>
<h2>Your Child Is Not Ready:</h2>
<p>If you don’t feel your child understands what it means to be water baptized, it’s totally appropriate to wait. Remember, water baptism does not save– that is done by your child trusting Jesus. During this time, let your child observe water baptism and talk to them about what it means. Go through this devotional with your child and spur them on in their personal friendship with Jesus.</p>
<p>If you have any questions, or would like to talk to someone about the process, please call myself, or Sue (775-848-3280).</p>

<h1>Water Baptism Devotional I’m Getting Baptized</h1>
<h2>Obeying Jesus’ Commandment</h2>
<p>Matthew 28:19-20</p>
<p>So you must go and make disciples of all nations. Baptize them in the name of the Father and of the Son and of the Holy Spirit. Teach them to obey everything I have commanded you. And you can be sure that I am always with you, to the very end.</p>
<p>Being baptized in water is one of the first steps you take in following Jesus. A friend of Jesus does not just learn about Jesus, but follows Jesus and does the things He did. The number one reason you should get baptized in water is because Jesus did and He told us to also.</p>
<p>When you became a friend of Jesus, you were brought out of the devil’s kingdom and put into God’s kingdom under Jesus’ control. That means you’ve decided to do what Jesus says and follow His example. Jesus was water baptized, so we should be too.</p>
<p>Take time right now to read about Jesus’ baptism in your Bible in Matthew 3:13 17.</p>
<blockquote>What is the number one reason you should be baptized in water?</blockquote>
<h2>An Outward Picture</h2>
<p>Colossians 2:12</p>
<p>When you were baptized, you were buried together with him. You were raised to life together with him by believing in God’s power. God raised Jesus from the dead.</p>
<p>Water baptism is just an outward picture of what Jesus has done for you on the inside. Jesus died for your sins, was buried, and then was raised from the dead! When you asked Jesus to be your forever friend, there was a death, a burial and a resurrection (that means raising back to life) that happened in your spirit as you prayed the prayer. (I Corinthians 12:13)</p>
<p>That’s why you go all the way under the water when you are baptized. You are showing people that your old spirit and all your past sins (the bad things you do) are buried with Jesus. When you come up out of the water, you are saying Jesus has given you new life. Everyone sees the picture of the change that has taken place inside your heart.</p>￼
<blockqoute>What is water baptism a picture of?</blockqoute>
<h2>A Decision of Faith</h2>
<p>Romans 6:4</p>
<p>By being baptized, we were buried with Christ (Jesus) into his death. Christ (Jesus) has been raised from the dead by the Father’s glory. And like Christ (Jesus) we can also life a new life.</p>
<p>Water baptism does not save you. It has no purpose or meaning until after a person has decided to follow Jesus and invited Jesus into his heart and life. But water baptism is a very important step in your new Christian life. When you decided to follow Jesus, you made a decision of faith to receive Him into your heart. When you are baptized in water, you are making a decision of faith to follow Him all the days of your life.</p>
<p>You are choosing to let Jesus and your new, born-again spirit be in charge of your life. Your water baptism will always be a reminder of your decision to follow the Lord.</p>
<blockqoute>When you are baptized in water, it means you have made a decision of faith to do what?</blockqoute>