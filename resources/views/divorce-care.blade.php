<h1>DivorceCare Sessions</h1>

<div class="half">
      <h2>What’s Happening to Me? — Session 1</h2>
      <p>This introductory seminar helps answer the question, “Why do I feel the way I do?” Participants will come out of this session with an overview of the DivorceCare program and an appreciation for the benefits of a support group environment.</p>

      <h2>The Road to Healing/Finding Help — Session 2</h2>
      <p>This video segment helps participants begin to identify the many losses that occurred as the result of their divorce. It also introduces strategies they can use to begin the process of healing from separation or divorce.</p>

      <h2>Facing My Anger — Session 3</h2>
      <p>This video seminar deals with the subject of anger, a nearly universal response to the pain and stress of divorce. Participants will gain constructive suggestions for dealing with their anger and learn methods for responding to the anger coming from an ex-spouse.</p>

      <h2>Facing My Depression — Session 4</h2>
      <p>Depression can be a paralyzing emotion for people going through divorce. Even so, it can also be a “healing emotion,” and this video seminar explores constructive responses participants can employ to deal with depression.</p>

      <h2>Facing My Loneliness — Session 5</h2>
      <p>After divorce, many people respond to their loneliness in ways that will cause them even deeper pain. This segment explores healthy ways to overcome the loneliness that will inevitably arise during the participants’ separation or divorce.</p>

      <h2>What Does the Owner’s Manual Say? — Session 6</h2>
      <p>This video seminar explores real-world answers from the Bible on issues related to separation, divorce, and remarriage, presented in an easily understandable format.</p>

      <h2>New Relationships — Session 7</h2>
      <p>The loneliness that comes with divorce puts people at risk when making decisions about new relationships. This session helps participants determine whether they are ready for a new relationship and how to get it off on the right foot if they are. </p>
</div>

<div class="half">
      <h2>Financial Survival — Session 8</h2>
      <p>Most people are stretched financially during divorce. This video segment offers practical help on how to survive and ways to deal with the many money issues that participants face during and after divorce.</p>

      <h2>KidCare — Session 9</h2>
      <p>This video seminar helps participants understand the effects of divorce on their children and offers practical suggestions for being an effective single parent. Participants will also learn how their children are processing the divorce and how they can help in their healing. </p>

      <h2>Single Sexuality — Session 10</h2>
      <p>How do you deal with sexuality after divorce? This important session will help participants understand sexuality from God’s perspective and see that it is possible to be single again and satisfied.</p>

      <h2>Forgiveness — Session 11</h2>
      <p>The hurt that comes with divorce is a barrier that prevents many people from forgiving their former spouse. This seminar shows participants why forgiveness is important and how they can begin the process of forgiving.</p>

      <h2>Reconciliation — Session 12</h2>
      <p>Reconciliation is one of the most misunderstood aspects of the divorce healing process. Participants will learn that reconciliation can happen even if their marriage is not restored and why it’s important to pursue reconciliation.</p>

      <h2>Moving On, Growing Closer to God — Session 13</h2>
      <p>How can God produce something good out of something as bad as divorce? This segment will show participants how to grow closer to God as they go through their divorce experience.</p>
</div>