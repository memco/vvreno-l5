<p><a class="button" href="{{ URL::to_action('events@index') }}">No, don't delete it.</a>
{{ Form::open(URI::current(),'DELETE') }}
	{{ Form::token() }}
	{{ Form::submit('Yes, delete this event') }}
{{ Form::close() }}