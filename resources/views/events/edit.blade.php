{{ Form::open(URI::current(),'PUT') }}
	{{ Form::token() }}

	<fieldset>
		<legend>Event Details</legend>

		<ol>
			<li>
				<label for="title">Title</label>
				{{ Form::text('title', Input::old('title',$event->title)) }}
			</li>
			<li>
				<label for="event_date">Date</label>
				<input type="datetime" id="event_date" class="date" name="event_date" value="{{ Input::old('event_date',$event->event_date->format('F j, Y g:i')) }}">
			</li>
			<li>
				<label for="location">Location</label>
				{{ Form::text('location', Input::old('location',$event->location)) }}
			</li>
			<li>
				<label for="description">Description</label>
				{{ Form::textarea('description', Input::old('description',$event->description)) }}
			</li>
		</ol>
	</fieldset>

	{{ Form::submit('Save') }}
{{ Form::close() }}