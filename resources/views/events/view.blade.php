<div class="event">
	<h1>{{ $event->title }}</h1>
	<p>{{ $event->event_date->format('F j, Y g:i A') }}</p>
	<p>{{ $event->location }}</p>
	<p>{{ $event->description }}</p>
	@if(Auth::check())
		<p>{{ HTML::link_to_action('events@edit', 'Edit', array($event->id)) }}</p>
		<p>{{ HTML::link_to_action('events@delete', 'Delete', array($event->id)) }}</p>
	@endif
</div>