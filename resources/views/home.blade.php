<div class="row">

@include('partials.vimeo', $videos)

<section id="main">
	{{-- @include('partials.slides', $slides) --}}

	{{-- @include('partials.blog', $blog) --}}
	{{-- @include('partials.facebook', $facebook) --}}
</section>

<section id="sidebar">
	<section id="info">
		<h1>Join Us</h1>
		<h2>Sundays</h2>
		<ol>
			<li><span class="event-title">Love in Action</span><span class="event-time"> 8:00 AM</span></li>
			<li><span class="event-title">Sunday School**</span><span class="event-time"> 9:15 AM</span></li>
			<li><span class="event-title">Worship Service**</span><span class="event-time"> 10:30 AM</span></li>
			<li><span class="event-title">Celebrate Recovery**</span><span class="event-time">6:00 PM</span></li>
		</ol>

		<h2>Tuesdays</h2>
		<ol>
			<li><span class="event-title">Royal Rangers (Meets in Annex)</span><span class="event-time">6:30 PM</span></li>
			<li><span class="event-title">Budgeting Class (Crown Ministries)**</span><span class="event-time">6:30 PM</span></li>
		</ol>

		<h2>Wednesdays</h2>
		<ol>
			<li><span class="event-title">Book and Cook**</span><span class="event-time">9:00 AM</span><br>(Ladies' Study and Fellowship)</li>
			<li><span class="event-title">Tribe**</span><span class="event-time">6:30 PM</span></li>
		</ol>

		<h2>Thursdays</h2>
		<ol>
			<li><span class="event-title">Divorce Care**</span><span class="event-time">6:00 PM</span></li>
			<li><span class="event-title">Small Group: The Book of Romans</span><span class="event-time">6:30 PM</span><br>Call ahead for childcare. Meets in the sanctuary.</li>
		</ol>

		<h2>Saturdays</h2>
		<ol>
			<li><span class="event-title">Men's Growth Breakfasts</span><span class="event-time">9:00 AM</span> (2nd Saturday)</li>
		</ol>

		<p>** Childcare is provided.</p>

		<div class="location" title="location">
			<h2><span class="icon" aria-hidden="true" data-icon="&#xf041;"></span><span class="visually-hidden">Location</span></h2>
			<p><a href="https://maps.google.com/maps?q=Valley+View+Christian+Church,+Geiger+Grade+Road,+Reno,+NV&amp;hl=en&amp;sll=37.0625,-95.677068&amp;sspn=52.637906,106.259766&amp;oq=VAlley+view+christian+church+reno&amp;hq=Valley+View+Christian+Church,&amp;hnear=Geiger+Grade+Rd,+Reno,+Nevada+89521&amp;t=m&amp;z=14">1805 Geiger Grade Rd.<br>
			Reno NV, 89511</a></p>
		</div>
	</section>

	{{-- @include('partials.tweets', $tweets) --}}

</section>
</div>