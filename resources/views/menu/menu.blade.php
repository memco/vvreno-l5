<{{ $menu->tag }}{{ $menu->attributes() }}>
	@foreach($menu->items as $item)
		@if($item->hasChildren())
			{{ view('menu.menu', ['menu'=>$item]) }}
		@else
			{{ view('menu.item', ['item'=>$item]) }}
		@endif
	@endforeach
</{{ $menu->tag}}>