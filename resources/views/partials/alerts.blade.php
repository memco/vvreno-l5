<section class="messages">
	@foreach( array('success'=>'&#xf058;','error'=>'&#xf071;','warning'=>'&#xf06a;','notice'=>'&#xf05a;') as $type=>$icon )
		@if( $messages->has( $type ))
			<div class="{{ $type }}">
				<h1 title="{{ studly_case( $type ) }}"><span aria-hidden="true" data-icon="{{ $icon }}"></span><span class="visually-hidden">{{ studly_case( $type ) }}</span></h1>
				@foreach( $messages->get( $type ) as $message )
					<p>{{ $message }}</p>
				@endforeach
			</div>
		@endif
	@endforeach
</section>