@if(isset($blog))
	<section id="blog">
		<h1>Blog</h1>
		@foreach($blog as $entry)
			<header>
				<div class="blog-date">
					<p class="date-month">{{ $entry->get_date('M Y') }}</p>
					<p class="date-day">{{ $entry->get_date('j') }}</p>
				</div>

				<h2><a href="{{ $entry->get_permalink() }}">{{ $entry->get_title() }}</a></h2>
			</header>
			<article>
				<p>{!! str_limit($entry->get_description(), 750, "…") !!}</p>
			</article>
			<p class="more-link"><a href="{{ $entry->get_permalink() }}">Read More <span data-icon="&#xf0da"></span></a></p>
		@endforeach
	</section>
@endif