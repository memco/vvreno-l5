@if(isset($events) && ! empty($events))
<section id="events">
	<h1>Events</h1>
	@foreach($events as $event)
		<div itemscope itemtype="http://schema.org/Event">
			<h2 itemprop="name"><a href="{{ URL::to_action('events@view', array($event->id)) }}">{{ $event->title }}</a></h2>
			{{ !empty($event->description) ? "<p>{$event->description}</p>" : '' }}
			{{ !empty($event->location) ? "<p>{$event->location}</p>" : '' }}
			<time itemprop="startDate" datetime="{{ $event->event_date->format('Y-m-d\Tg:i') }}">{{ $event->event_date->format('F j Y g:i A') }}</time>
		</div>
	@endforeach
	@if(Auth::check())
		<a href="{{ URL::to_action('events@add') }}" title="Add new event" class="button"><span data-icon="&#xf067"></span> New</a>
	@endif