<section id="facebook">
	<h1>Facebook</h1>
	@foreach($facebook as $post)
		<article style="border-bottom: 1 px solid #000 !important;">
			{!! vvreno\Feed::resizeFacebookImages($post->get_content()) !!}
		</article>
	@endforeach
</section>