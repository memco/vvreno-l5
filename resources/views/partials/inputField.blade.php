@if(isset($field))
	<label for="{!! $field !!}">{!! $label or Str::title($field) !!}</label>
	<input type="{!! $type or "text" !!}"@foreach($attributes as $attribute=>$value){!! $attribute !!}="{!! $value !!}"@endforeach>
@endif