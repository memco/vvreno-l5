@if(isset($slides))
	<section id="slideshow" class="bss-slides num1" tabindex="1" autofocus="autofocus">
		@foreach($slides as $slide)
			<figure>
				{!! $slide->link ? "<a href=\"$slide->link\">" : '' !!}<img src="{!! $slide->url . '?w=727' !!}">{!! $slide->link ? "</a>" : '' !!}
				<figcaption>
					{!! $slide->heading ? "<h1>$slide->heading</h1>" : '' !!}
					{!! $slide->description ? "<p>$slide->description</p>" : '' !!}
				</figcaption>
			</figure>
		@endforeach
	</section>
@endif