@if(!empty($tweets))
<section id="tweets">
	<h1>Tweets</h1>
	<ul>
		@foreach($tweets as $tweet)
			<li><a href="{!! Twitter::linkTweet($tweet) !!}">{!! Twitter::linkify($tweet->text) !!}</a> <span>{!! Twitter::ago($tweet->created_at) !!}</span></li>
		@endforeach
	</ul>
</section>
@endif