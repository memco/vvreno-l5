@if(!empty($videos))
<section id="vimeo">
	{{-- <h1>Videos</h1> --}}
	@foreach($videos as $video)
		<?php Debugbar::log($video); ?>
		<?php
			$url = $video->get_link();
			$url = explode('/', $url);
			$url = end($url);
		?>
		<div class="video-container" style="position: relative;padding-bottom: 75%; /* 16:9 */	padding-top: 25px;height: 0;">
			<iframe src="//player.vimeo.com/video/{{ $url }}" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
	@endforeach
</section>
@endif