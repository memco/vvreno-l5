<h1>Pastors</h1>
<section>
<h2>John and Sue McKendricks</h2>
<p><img src="{{ asset('img/mckendricks-pastors-255x191.jpg') }}" class="pastor-photo">John holds a M.Div. from the King's University in Van Nuys CA. where he is also finishing his D.Min.. </p>
<p>John and Sue have been married for over 30 years. They have several adult children, Sean, Nicole, Michael and Emily.</p>
<p>John has almost thirty–years of ministry experience; beginning with his conversion in 1979. John also performed with a Christian band before accepting his first Pastorate in 1994 and then went on to plant the first of three churches in 1997. John severed in several positions with The King's University with Dr. Jack Hayford in Los Angels and left his position as Dean of Students and Chaplain coming to Multnomah University and Reno NV in 2009.</p>
<p>John loves music in all shapes and sizes; any movie that includes explosions and is an avid Baseball and Football fan, though he can be talked into watching any sport. He is also a renown foodie which is why he loves <em>Iron Chef</em> and <em>Diners, Drive ins and Dives</em>. He believes that Guy Feri should retire and give John the convertible Camero and the Cable Show. </p>
</section>