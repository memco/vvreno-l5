<p><a class="button" href="{{ URL::to('sermons') }}">No, don't delete it.</a>
{{ Form::open(URI::current(),'DELETE') }}
	{{ Form::token() }}
	{{ Form::submit('Yes, delete this sermon') }}
{{ Form::close() }}