<h1>Sermons</h1>
@if(! empty($sermons))
{{-- {!! $sermons->render() !!} --}}
<table class="data-list">
	<thead>
		<tr>
			<th>Title</th>
			<th>Speakers</th>
			<th>Preached on</th>
			<th>Duration</th>
			<th>Size</th>
			@if(Auth::user())
				<th>Edit</th>
				<th>Delete</th>
			@endif
		</tr>
	</thead>
	<tbody>
		@foreach($sermons as $sermon)
		<tr class="sermon">
			<td><a href="{{ $sermon->url }}">{{ $sermon->title }}</a></td>
			<td>{{ $sermon->speakers }}</td>
			<td>{{ $sermon->preached_on->format('F j, Y') }}</td>
			<td>{{ $sermon->length->format('%H:%I:%S') }}</td>
			<td>{{ $sermon->size }}</td>
			@if(Auth::user())
				<td>{{ HTML::action('sermon@edit', 'Edit', [$sermon->id]) }}</td>
				<td>{{ HTML::action('sermon@delete', 'Delete', [$sermon->id]) }}</td>
			@endif
		</tr>
		@endforeach
	</tbody>
</table>
@else
	<p>No sermons available at this time.  Please check back later.</p>
@endif
@if(Auth::user())
	<p><a href="{{ action('sermon@add') }}" title="Create new sermon">Add new sermon</a>
@endif