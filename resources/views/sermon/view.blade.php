<div class="sermon">
	<h1>{{ $sermon->title }}</h1>
	<p>{{ $sermon->speakers }}</p>
	<p>{{ $sermon->preached_on->format('F j, Y') }}</p>
	<p>{{ $sermon->length->format('%H:%I:%S') }}</p>
	<p>{{ $sermon->size }}</p>
	<pre>{{ $sermon->description }}</pre>
	@if(Auth::user())
		<p>{{ HTML::link_to_action('sermon@edit', 'Edit', array($sermon->id)) }}</p>
		<p>{{ HTML::link_to_action('sermon@delete','Delete', array($sermon->id)) }}</p>
	@endif
</div>