{!! Form::open(['files'=>true,'url'=>url('slides'), 'method'=>'POST']) !!}
<fieldset>
  <legend>New Slide</legend>
  <ol>
    <li{!! $errors->has('file') ? ' class="error"' : '' !!}>
      {!! Form::label('file','File') !!}
      {!! Form::file('file') !!}
      @if($errors->has('file'))
        <ul class="input-advice">
          @foreach($errors->get('file') as $error)
            <ul>{!! $error !!}</ul>
          @endforeach
        </ul>
      @endif
    </li>
    <li{!! $errors->has('url') ? 'class="error"' : '' !!}>
      {!! Form::label('url',' Or URL') !!}
      {!! Form::url('url') !!}
    </li>
    <li{!! $errors->has('heading') ? ' class="error"' : '' !!}>
      {!! Form::label('heading','Title') !!}
      {!! Form::text('heading', Input::old('heading')) !!}
    </li>
    <li{!! $errors->has('description') ? ' class="error"' : '' !!}>
      {!! Form::label('description','Description') !!}
      {!! Form::textarea('description', Input::old('description')) !!}
    </li>
    <li{!! $errors->has('link') ? ' class="error"' : '' !!}>
      {!! Form::label('link','Link') !!}
      {!! Form::url('link', Input::old('link')) !!}
      <p class="input-advice">If you enter a URL, the slide will become a link users can click.</p>
    </li>
    <li>
      {!! Form::submit('Upload') !!}
    </li>
  </ol>
</fieldset>
{!! Form::close() !!}