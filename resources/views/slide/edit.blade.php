<p><img src="{!! $slide->url !!}"> {!! $slide->heading !!}</p>
{!! Form::open(['url'=>action('SlidesController@update'), 'method'=>'PUT', 'files'=>true]) !!}
<fieldset>
  <legend>New Slide</legend>
  {!! Form::token() !!}
  <ol>
    <li{!! $errors->has('file') ? ' class="error"' : '' !!}>
      {!! Form::label('file','File') !!}
      {!! Form::file('file') !!}
      <p class="input-advice">Use this field to replace the current image.</p>
    </li>
    <li{!! $errors->has('heading') ? ' class="error"' : '' !!}>
      {!! Form::label('heading','Title') !!}
      {!! Form::text('heading', Input::old('heading', $slide->heading)) !!}
    </li>
    <li{!! $errors->has('description') ? ' class="error"' : '' !!}>
      {!! Form::label('description','Description') !!}
      {!! Form::textarea('description', Input::old('description', $slide->description)) !!}
    </li>
    <li{!! $errors->has('link') ? ' class="error"' : '' !!}>
      {!! Form::label('link','Link') !!}
      {!! Form::url('link', Input::old('link', $slide->link)) !!}
      <p class="input-advice">If you enter a URL, the slide will become a link users can click.</p>
    </li>
    <li>
      {!! Form::submit('Upload') !!}
    </li>
  </ol>
</fieldset>
{!! Form::close() !!}