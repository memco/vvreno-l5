<h1>Slides</h1>
@forelse($slides as $slide)
<div class="slide">
  {{-- <h1><a href="{{ $slide->url }}">{{ $slide->heading ?: str_replace('_',' ',pathinfo(parse_url($slide->url, PHP_URL_PATH), PATHINFO_FILENAME)) }}</a></h1> --}}
  <p><img src="{{ $slide->url.'?w=345' }}"></p>
  <p>{{ $slide->description }}</p>
  <p>{{ $slide->link }}</p>
  @if(Auth::user())
    <div class="actions">
        <p><a href="{{ action('SlidesController@edit', array($slide->id)) }}">Edit</a></p>
        <p><a href="{{ action('SlidesController@delete', array($slide->id)) }}">Delete</a></p>
    </div>
  @endif
</div>
@empty
  No slides available at this time.  Please check back later.
@endforelse
<p id="add-link"><a href="{{ action('SlidesController@create') }}">Add new</a></p>