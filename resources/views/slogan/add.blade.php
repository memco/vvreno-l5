{!! Form::open() !!}
    <fieldset>
        <legend></legend>
        {!! Form::token() !!}
        <ol>
            <li>
                {!! Form::label('slogan','Slogan')!!}
                {!! Form::text('slogan', Input::old('slogan')) !!}
            </li>
            <li>
                {!! Form::submit('Save') !!}
            </li>
        </ol>
    </fieldset>
{!! Form::close() !!}