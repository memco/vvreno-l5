{!! Form::model($slogan, ['url'=>'slogan', 'method'=>'put']) !!}
    <fieldset>
        <legend></legend>
        {!! Form::token() !!}
        <ol>
            <li>
                {!! Form::label('slogan','Slogan')!!}
                {!! Form::text('slogan') !!}
            </li>
            <li>
                {!! Form::submit('Save') !!}
            </li>
        </ol>
    </fieldset>
{!! Form::close() !!}